# Cryptographic Hash Scripts 

Powershell and Bash scripts for calculating Cryptographic Hash for downloaded files

Two versions of the same concept - given a physical file and an expected cryptographic hash value, make sure the files hash value agrees with the expected value.

Files in this project:

README.md : This file

check_SHA512.ps1 : PowerShell version of the script to calculate the 512 bit SHA-2 hash of a specified file and compare it to an expected value.
                   Expected SHA2-512 hash: 410DFB6BF838D3688018C3C101B2AAA2AD2FE440DDF4E620FD8377A8FB0154F6ECD736B637AF08FE558C6C0F405013A542F2718618414A652912F08F7A6C3EA6

check_SHA512.sh :  Bash shell script version of the script to calculate the 512 bit SHA-2 hash hash of a specified file and compare it to an expected value.
                   Expected SHA2-512 hash: AFFB1B0D28EAD107714E930AAFC08D65F0FA9FA347188E9620CF1D110E91FE25F006C317C60252232CB8A1D176DB8D79507B4BB8A807247010925EBAB655F07B

No warranty is offered or implied by using these scripts. Use them at your own risk. If you believe you've discovered a mistake with the scripts, please feel free to inform us.