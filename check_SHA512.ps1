# Script Name: check_SHA512.ps1
# Author : Nick
# Date: 16 July 2019
# Purpose: Quickly compare the SHA512 calculated hash of downloaded
# file with the hash provided by another party (the author hopefully).
# Usage: The script will prompt for the name of a downloaded
# this can include the path and for the hash value provided by the
# source file. The script will confirm whether they agree or otherwise.
# Command line usage:
# .\check_SHA512.ps1 -file myFile.txt -hash 3dff9bc9283d34a983b…
# Warning: This script only confirms that you have received what a remote
# site considers to be a true copy of a file, you should still treat the file
# with suspicion (eg malware or virus risk) if you do not know the author.

Param (
  [string]$file = $( Read-Host "Enter the filename you've received" ),
  [string]$hash = $( Read-Host "Enter the hash value you copied from the remote source" )
)

# Just wanted to manage the parameters with more meaningful names
if (-not ([string]::IsNullOrEmpty($file))) {
  $receivedFilename = $file
}

if (-not ([string]::IsNullOrEmpty($hash))) {
  $remoteHash = $hash
}

try {
  $calculatedHash = ((Get-FileHash $receivedFilename -Algorithm SHA512 -ErrorAction Stop) | Select -Expand "Hash" ) 
}
catch {
  Write-Host "There was an error calculating the hash for the file you specified.`nCheck the file exists and try again."
  break
}

# Strings in PowerShell are case insensitive, this is just for formatting.
$calculatedHash = $calculatedHash.ToUpper()
$remoteHash = $remoteHash.ToUpper()

if ($calculatedHash -eq $remoteHash) {
  Write-Host "The hashes agree, the downloaded file appears to be the same as the source.`nNow scan the file for malware and viruses."
} else {
  Write-Host "The hashes did NOT agree. Do NOT open the file until you've resolved why."
  Write-Host "The remote hash was : $remoteHash"
  Write-Host "The local hash was : $calculatedHash"
}