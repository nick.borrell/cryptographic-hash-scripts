#!/bin/bash
# Script Name: check_SHA512.sh
# Author: Nick
# Date Written: 18 June 2019
# Purpose: Compare the checksum of a downloaded file against a received value.
#
# Usage: The script can be invoked using parameters, but if it doesn't receive them
# it will prompt for what it requires.
#
# Command line usage:
# ./check_SHA512.sh -f myFile.txt -h 3dff9bc9283d34a983b...
#
# Warning: This script only confirms that you have received what a remote site
# considers to be a true copy of a file, you should still treat the file with
# suspicion (eg malware or virus risk) if you do not know the author.
#

# Test the parameters, if we get more than expected then stop

if [ $# -gt 4 ]; then
  echo -e "Unexpected parameters\n"
  echo "Expected:"
  echo "./check_hash.sh -f myFile.txt -h 3dff9bc9283d34a983b..."
  echo "If you call ./check_hash.sh without parameters it will prompt you for what it needs"
  exit
else
  while getopts f:h: option
  do
  case "$option"
  in
  f) filename=${OPTARG};;
  h) remoteHash=${OPTARG};;
  *) echo "Unexpected argument"; exit;;
  esac
  done
fi

if [ -z "$filename" ]; then
  read -p "Enter the filename you've received: " filename
fi

if [ -z "$remoteHash" ]; then
  read -p "Enter the SHA512 has you received: " remoteHash
fi

# Now that we have a filename, let's make sure it exists, and quit if it doesn't
# And if it exists, let's make sure it's not a directory

if [ ! -f "$filename" ]; then
  echo "Couldn't find a file named:  $filename"
  exit
fi

# Ok - we can now see what hash we get from the file, using sed to just get  the hash
 calculatedHash=$(openssl sha -sha512 $filename | sed 's/^.* //')

# Let's make sure bash our hash and the received hash are upper case before comparing
calculatedHash=$(echo $calculatedHash | tr a-z A-Z)
remoteHash=$(echo $remoteHash | tr a-z A-Z)

# Finally, we can compare the hashes and give a result
if [ "$calculatedHash" = "$remoteHash" ]; then
  echo -e  "The hashes agree, the downloaded file appears to be the same as the source.\nNow scan the file for malware and viruses."
else
  echo "The hashes did NOT agree. Do NOT open the file until you've resolved why."
  echo "The remote hash was: $remoteHash"
  echo "The local hash was: $calculatedHash"
fi

# Throw in an extra newline to make it easier to see this has completed
echo
exit